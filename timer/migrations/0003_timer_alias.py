# Generated by Django 3.0.5 on 2020-05-01 04:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timer', '0002_auto_20200501_0415'),
    ]

    operations = [
        migrations.AddField(
            model_name='timer',
            name='alias',
            field=models.CharField(blank=True, help_text='A nickname for the timer, for easier reference', max_length=30, null=True, verbose_name='Timer alias'),
        ),
    ]
