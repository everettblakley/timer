from django.contrib import admin

from .models import Timer, Session, Break

admin.site.register(Timer)
admin.site.register(Session)
admin.site.register(Break)
