from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from datetime import timedelta

default_break_time = timedelta(0)

class Timer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    length = models.DurationField(verbose_name="Timer Length", null=False, blank=False, default=timedelta(0))
    break_length = models.DurationField(verbose_name="Timer break length", default=default_break_time)
    stop_after_break_count = models.IntegerField(
        verbose_name="Stop Timer After Breaks",
        help_text="The number of elapsed breaks before the timer session stops",
        default=1)
    alias = models.CharField(_("Timer alias"), max_length=30, help_text=_("A nickname for the timer, for easier reference"), null=True, blank=True)

    class Meta:
        verbose_name = _("Timer")
        verbose_name_plural = _("Timers")

    def __str__(self):
        return self.alias if self.alias else "{0}, {1}, {2}".format(str(self.length), str(self.break_length), self.stop_after_break_count)

    def get_absolute_url(self):
        return reverse("Timer_detail", kwargs={"pk": self.pk})

class Session(models.Model):
    timer = models.ForeignKey(Timer, on_delete=models.CASCADE)
    start = models.DateTimeField(verbose_name="Timer session start time", auto_now=True)
    end = models.DateTimeField(verbose_name="Timer session end time", blank=True, null=True)

    class Meta:
        verbose_name = _("Session")
        verbose_name_plural = _("Sessions")

    def __str__(self):
        return str(self.start)

    def get_absolute_url(self):
        return reverse("Session_detail", kwargs={"pk": self.pk})

class Break(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    start = models.DateTimeField(verbose_name="Break start time")
    length = models.DurationField(verbose_name="Length of break", default=default_break_time)
    
    class Meta:
        verbose_name = _("Break")
        verbose_name_plural = _("Breaks")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Break_detail", kwargs={"pk": self.pk})
