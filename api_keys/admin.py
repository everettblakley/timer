from django.contrib import admin
from .models import ApiKey
from django.http import HttpResponse

class ApiKeyAdmin(admin.ModelAdmin):
    readonly_fields = ["key", "user", "created_date"]
    
    def save_model(self, request, instance, form, change):
        user = request.user 
        instance = form.save(commit=False)
        if instance.user != user and not (user.is_superuser or user.is_staff):
            return HttpResponse('Unauthorized', status=401)
        if not change or not instance.user:
            instance.user = user
        instance.save()
        form.save_m2m()
        return instance

admin.site.register(ApiKey, ApiKeyAdmin)
