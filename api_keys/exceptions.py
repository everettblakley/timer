class Error(Exception):
    """Base error class"""
    pass

class NotExistApiKeyError(Error):
    """Raised when the API does not exist"""
    pass

class ExpiredApiKeyError(Error):
    """Raised when the API Key exists, but is expired"""
    pass