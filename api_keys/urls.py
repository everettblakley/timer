from django.urls import path
from . import views

urlpatterns = [
    path('api/api_keys/all/', views.ApiKeyList.as_view()),
    path('api/api_keys/', views.ApiKeyListForUser.as_view())
]
