from django.utils import timezone
from .models import ApiKey
from django.core.exceptions import ObjectDoesNotExist
from uuid import UUID

def ApiKeyIsValid(apiKey):
    """
    Determines if the provided API key is valid, which occurs if it exists, and if there is no key newer than the provided key
    """
    existingKeys = ApiKey.objects.all()
    try:
        key = existingKeys.get(key=apiKey)
        newerKeys = existingKeys.filter(user=key.user).filter(created_date__gt=key.created_date)
        return len(newerKeys) == 0
    except ObjectDoesNotExist:
        return False
