from django.db import models
from django_currentuser.middleware import (get_current_user, get_current_authenticated_user)
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
import uuid
from django.utils import timezone

class ApiKey(models.Model):
    key = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, 
        editable=False,
        on_delete=models.CASCADE, 
        null=False, 
        blank=True, 
        help_text="This defaults to you")
    created_date = models.DateTimeField(auto_now_add=True)

    def clean(self, *args, **kwargs):
        # custom validation login here
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("ApiKey")
        verbose_name_plural = _("ApiKeys")

    def __str__(self):
        return str(self.key)

    def get_absolute_url(self):
        return reverse("ApiKey_detail", kwargs={"pk": self.pk})
