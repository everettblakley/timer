from .models import ApiKey
from .serializers import ApiKeySerializer
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from django_currentuser.middleware import get_current_authenticated_user

class ApiKeyList(generics.ListAPIView):
    queryset = ApiKey.objects.all()
    serializer_class = ApiKeySerializer
    permission_classes = [IsAdminUser]

class ApiKeyListForUser(generics.ListCreateAPIView):
    serializer_class = ApiKeySerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return ApiKey.objects.filter(user=self.request.user)
