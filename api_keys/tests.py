from django.test import TestCase, Client
from .models import ApiKey
from django.contrib.auth.models import User
from django_currentuser.middleware import get_current_authenticated_user
from uuid import uuid4

from .utils import ApiKeyIsValid

test_user = {
    'username': 'test_user',
    'password': 'password'
}

test_superuser = {
    'username': 'test_superuser',
    'password': 'password'
}

class ApiKeysTestCase(TestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser(test_superuser['username'], password=test_superuser['password'])
        self.user = User.objects.create_user(test_user['username'], password=test_user['password'])
        self.client = Client()
        
    def test_users_can_create_api_key(self):
        """Tests that users can create api keys"""
        self.client.login(username=test_user['username'], password=test_user['password'])
        ApiKey.objects.create(user=self.user)

    def test_users_cannot_create_api_keys_for_other_users(self):
        """Tests that a regular user cannot create an API Key for another user"""
        self.client.login(username=test_user['username'], password=test_user['password'])
        self.assertRaises(BaseException, ApiKey.objects.create(user=self.superuser))

    def test_superusers_can_create_api_keys_for_other_users(self):
        """Tests that superusers can create API keys for other users"""
        self.client.login(username=test_superuser['username'], password=test_superuser['password'])
        ApiKey.objects.create(user=self.user)
        
class UtilsTestCase(TestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser(test_superuser['username'], password=test_superuser['password'])
        self.user = User.objects.create_user(test_user['username'], password=test_user['password'])
        self.client = Client()

    def test_no_api_keys_exists_returns_false(self):
        fake_key = uuid4()
        self.assertFalse(ApiKeyIsValid(fake_key))

    def test_no_other_keys_for_user_returns_true(self):
        self.client.login(username=test_user['username'], password=test_user['password'])
        key = ApiKey.objects.create(user=self.user)
        self.assertTrue(ApiKeyIsValid(str(key.key)))

    def test_no_keys_created_after_returns_true(self):
        self.client.login(username=test_user['username'], password=test_user['password'])
        key1 = ApiKey.objects.create(user=self.user)
        key2 = ApiKey.objects.create(user=self.user)
        self.assertFalse(ApiKeyIsValid(str(key1.key)))
        self.assertTrue(ApiKeyIsValid(str(key2.key)))

    def test_other_users_keys_have_no_effect(self):
        self.client.login(username=test_superuser['username'], password=test_superuser['password'])
        key1 = ApiKey.objects.create(user=self.user)
        key1 = ApiKey.objects.create(user=self.superuser)
        self.assertTrue(ApiKeyIsValid(str(key1.key)))
        